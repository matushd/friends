-- MySQL dump 10.13  Distrib 5.6.24, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: friendship_db
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `connection`
--

DROP TABLE IF EXISTS `connection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connection` (
  `uid_one` bigint(20) unsigned NOT NULL,
  `uid_two` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`uid_one`,`uid_two`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `friendship_db`.`connection_BEFORE_INSERT` BEFORE INSERT ON `connection` FOR EACH ROW
BEGIN
	DECLARE uid_tmp BIGINT DEFAULT 0;
	DECLARE msg varchar(255);
    IF NEW.uid_one <= 0 OR NEW.uid_two <= 0 THEN
		SET msg = concat('connection_BEFORE_INSERT: Trying to insert negative uid!');
		SIGNAL SQLSTATE '45000' SET message_text = msg;
    END IF;
	IF NEW.uid_one > NEW.uid_two THEN
		SET uid_tmp = NEW.uid_two;
		SET NEW.uid_two = NEW.uid_one; 
		SET NEW.uid_one = uid_tmp;
	ELSEIF NEW.uid_one = NEW.uid_two THEN
		SET msg = concat('connection_BEFORE_INSERT: Trying to insert two the same uid: ', CAST(NEW.uid_one as char));
		SIGNAL SQLSTATE '45000' SET message_text = msg;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `friendship_db`.`connection_BEFORE_UPDATE` BEFORE UPDATE ON `connection` FOR EACH ROW
BEGIN
	DECLARE uid_tmp BIGINT DEFAULT 0;
	DECLARE msg varchar(255);
    IF NEW.uid_one <= 0 OR NEW.uid_two <= 0 THEN
		SET msg = concat('connection_BEFORE_UPDATE: Trying to update negative uid! ', CAST(NEW.uid_one as char), ' and ',CAST(NEW.uid_two as char));
		SIGNAL SQLSTATE '45000' SET message_text = msg;
    END IF;
	IF NEW.uid_one >= NEW.uid_two  THEN
		SET uid_tmp = NEW.uid_two;
		SET NEW.uid_two = NEW.uid_one; 
		SET NEW.uid_one = uid_tmp;
    END IF;
    IF OLD.uid_one >= NEW.uid_two AND !NEW.uid_one THEN
		SET NEW.uid_two = OLD.uid_one; 
		SET NEW.uid_one = NEW.uid_two;
    END IF;
    IF NEW.uid_one >= OLD.uid_two AND !NEW.uid_two THEN
		SET NEW.uid_two = NEW.uid_one; 
		SET NEW.uid_one = OLD.uid_two;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'friendship_db'
--

--
-- Dumping routines for database 'friendship_db'
--
/*!50003 DROP PROCEDURE IF EXISTS `add_conn` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_conn`(IN first_uid INT, IN second_uid INT)
BEGIN
	DECLARE exit handler for sqlexception
	  BEGIN
		-- ERROR
	  ROLLBACK;
      SELECT 'ERROR!' as msg, -200 as res;
	END;

	DECLARE exit handler for sqlwarning
	 BEGIN
		-- WARNING
	 ROLLBACK;
     SELECT 'WARNING!' as msg, -100 as res;
	END;
    
    DECLARE CONTINUE HANDLER FOR 1062
	SELECT 'ALREADY EXIST!' as msg, -1 as res;
    
    START TRANSACTION;
	  INSERT INTO connection (uid_one, uid_two) values (first_uid, second_uid);
      IF ROW_COUNT() > 0 THEN SELECT 'OK!' as msg, ROW_COUNT() as res; END if;
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_conn` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_conn`(IN first_uid INT, IN second_uid INT)
BEGIN
	DECLARE uid_o, uid_t INT;
	DECLARE exit handler for sqlexception
	  BEGIN
		-- ERROR
	  ROLLBACK;
      SELECT 'ERROR!' as msg, -200 as res;
	END;

	DECLARE exit handler for sqlwarning
	 BEGIN
		-- WARNING
	 ROLLBACK;
     SELECT 'WARNING!' as msg, -100 as res;
	END;
    
    if first_uid > second_uid then
		set uid_o = second_uid;
        set uid_t = first_uid;
	else
		set uid_o = first_uid;
        set uid_t = second_uid;
    end if;
    
    START TRANSACTION;
	  DELETE FROM connection where uid_one = uid_o AND uid_two = uid_t;
      IF ROW_COUNT() > 0 THEN SELECT 'OK!' as msg, ROW_COUNT() as res;
      else select 'Nothing deleted!' as msg, -3 as res;
      END if;
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_conns` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_conns`(IN uid INT, IN packNo INT, IN packSize INT)
BEGIN
	DECLARE pageNo INT;
	SET pageNo = (packNo-1)*packSize;
	select * FROM(
		select uid_one as uid from connection where uid_two = uid
        UNION
        select uid_two as uid from connection where uid_one = uid
    ) s limit pageNo,packSize;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-03 23:23:44
