<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Emde\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Connection Entity
 *
 * @author emde
 */
class Connection {
    
    /**
     *
     * @var int 
     */
    private $uidOne;
    
    /**
     *
     * @var int 
     */
    private $uidTwo;
    
    /**
     * 
     * @return int
     */
    function getUidOne() {
        return $this->uidOne;
    }

    /**
     * 
     * @return int
     */
    function getUidTwo() {
        return $this->uidTwo;
    }

    /**
     * 
     * @param int $uidOne
     */
    function setUidOne($uidOne) {
        $this->uidOne = (int) $uidOne;
    }

    /**
     * 
     * @param int $uidOne
     */
    function setUidTwo($uidTwo) {
        $this->uidTwo = (int) $uidTwo;
    }
    
    /**
     * 
     * @param ClassMetadata $metadata
     */
    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('uidOne', new Assert\GreaterThan(array('value' => 0)));
        $metadata->addPropertyConstraint('uidTwo', new Assert\GreaterThan(array('value' => 0)));
        $metadata->addConstraint(new Assert\Expression(array(
            'expression' =>  "this.getUidOne() !== this.getUidTwo()",
            "message" => "uidOne should be different than uidTwo!")));
    }

}
