<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Emde\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Emde\Repository\RepositoryInterface;
use Emde\Entity\Connection;
use Symfony\Component\HttpFoundation\Response;
/**
 * ConnectionController
 *
 * @author emde
 */
class ConnectionController {
    
    /**
     *
     * @var RepositoryInterface 
     */
    protected $repo;
    
    /**
     *
     * @var Symfony\Component\Serializer\Serializer 
     */
    protected $serializer;

    /**
     * 
     * @param RepositoryInterface $repo
     * @param Serializer $serializer
     */
    public function __construct(RepositoryInterface $repo, $serializer)
    {
        $this->repo = $repo;
        $this->serializer = $serializer;
    }
    
    /**
     * Get connections action
     * 
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request)
    {
        $ret['code'] = 200;
        $uid = (int) $request->attributes->get('uid_one');
        $format = $request->getRequestFormat();
        
        if($uid < 1) {
            $ret = array(
                'msg' => "Uid should be greather than 0!",
                'res' => '',
                'code' => 400
            );
        } else {
            try {
                $ret['res'] = $this->repo->getAllById($uid);
                $ret['code'] = 200;
                $ret['msg'] = "OK!";
            } catch (Exception $ex) {
                $ret = array('msg' => $ex->getMessage(), 'res' => '', 'code' => 500);
            }
        }
        
        $response = $this->prepareResponse($ret, $format, $request->getMimeType($format));
        if($ret['code'] == 200) {
            $response->setPublic();
            $response->setMaxAge(5);
            $response->setEtag(md5(serialize($ret)));
            $response->isNotModified($request);
        }
        
        return $response;
    }
    
    /**
     * Add connection action
     * 
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function addAction(Request $request, Application $app)
    {
        $format = $request->getRequestFormat();
        $connection = new Connection();
        $connection->setUidOne($request->get('uid_one'));
        $connection->setUidTwo($request->get('uid_two'));
        
        $errors = $app['validator']->validate($connection);
        if ($errors->count()) {
            $ret = array('msg' => $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage(), 
                'res' => '', 'code' => 400);
        } else {
            try {
                $ret = $this->repo->save($connection);
                $ret['code'] = $ret['res'] > 0 ? 201 : 200;
                $ret['res'] = '';
            } catch (Exception $ex) {
                $ret = array('msg' => $ex->getMessage(), 'res' => '', 'code' => 500);
            }
        }
        
        return $this->prepareResponse($ret, $format, $request->getMimeType($format));
    }
    
    /**
     * Delete connection action
     * 
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function deleteAction(Request $request, Application $app)
    {
        $format = $request->getRequestFormat();
        $connection = new Connection();
        $connection->setUidOne($request->get('uid_one'));
        $connection->setUidTwo($request->get('uid_two'));
        
        $errors = $app['validator']->validate($connection);
        if ($errors->count()) {
            $ret = array('msg' => $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage(), 
                'res' => '', 'code' => 400);
        } else {
            try {
                $ret = $this->repo->delete($connection);
                $ret['code'] = $ret['res'] > 0 ? 204 : 200;
                $ret['res'] = '';
            } catch (Exception $ex) {
                $ret = array('msg' => $ex->getMessage(), 'res' => '', 'code' => 500);
            }
        }
        
        return $this->prepareResponse($ret, $format, $request->getMimeType($format));
    }
    
    /**
     * Test site
     * 
     * @param Application $app
     * @return Response
     */
    public function testAction(Application $app) {
        return $app['twig']->render('test.html.twig');
    }
    
    /**
     * Prepare response
     * 
     * @param array $ret
     * @param string $format
     * @param string $mimetype
     * @return Response
     */
    private function prepareResponse($ret, $format, $mimetype) {
        return new Response($this->serializer->serialize($ret, $format), $ret['code'], 
                array("Content-Type" => $mimetype));
    }
}
