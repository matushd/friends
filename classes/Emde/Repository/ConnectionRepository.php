<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Emde\Repository;
 
use Doctrine\DBAL\Connection as DbalConnection;
use Emde\Entity\Connection;

/**
 * ConnectionRepository
 *
 * @author emde
 */
class ConnectionRepository implements RepositoryInterface {
    
    /**
     *
     * @var DbalConnection 
     */
    protected $db;
 
    /**
     * 
     * @param DbalConnection $db
     */
    public function __construct(DbalConnection $db)
    {
        $this->db = $db;
    }
    
    /**
     * Get all connections by id
     * 
     * @param int $uid
     * @param int $pack
     * @param int $packSize
     * @return array
     */
    public function getAllById($uid, $pack = 1, $packSize = 20) {
        $procedure = "CALL get_conns(?,?,?)";
        $inputParams = array((int) $uid, (int) $pack, (int) $packSize);
        $res = $this->db->fetchAll($procedure, $inputParams);
        return array_column($res, 'uid');
    }
    
    /**
     * Add new connection
     * 
     * @param Connection $connection
     * @return array
     */
    public function save(Connection $connection) {
        $procedure = "CALL add_conn(?,?)";
        $inputParams = array($connection->getUidOne(), $connection->getUidTwo());
        return $this->db->fetchAssoc($procedure, $inputParams);
    }
    
    /**
     * Delete connection
     * 
     * @param Connection $connection
     * @return array
     */
    public function delete(Connection $connection) {
        $procedure = "CALL del_conn(?,?)";
        $inputParams = array($connection->getUidOne(), $connection->getUidTwo());
        return $this->db->fetchAssoc($procedure, $inputParams);
    }
}
