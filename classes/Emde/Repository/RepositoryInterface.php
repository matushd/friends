<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Emde\Repository;

use Emde\Entity\Connection;
/**
 * RepositoryInterface
 *
 * @author emde
 */
interface RepositoryInterface {
    public function getAllById($id);
    public function save(Connection $connection);
    public function delete(Connection $connection);
}
