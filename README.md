# Friends list API #

Rest API was written in PHP and is based on the Silex micro framework, which is perfect for small applications.
The application allows you to return json or xml responses depending on the input parameters.
The application stores friends list similar to facebook.

## Application structure ##
```
.
├── classes // controllers, repositories, dbal sqlrelay adapter
├── composer.json
├── database //db dump + sqlrelay config
├── vendor //external libraries
├── views //test app views
└── web //document root + config + routing
```

## Actions ##

### Get contact list ###
```
GET http://localhost/get.{_format}/{uid_one}
```
* _format - response format (xml or json)
* uid_one - user id
e.q. http://localhost/get.json/1

### Add contact ###
```
POST http://localhost/add.{_format}
```
* _format - response format (xml or json)
Request body params:
* uid_one - user id
* uid_two - id of user that will by add to contact list
e.g. http://localhost/add.json

### Delete contact ###
```
DELETE http://localhost/delete.{_format}
```
* _format - response format (xml or json)
Request body params:
* uid_one - user id
* uid_two - id of user that will by delete to contact list
e.g. http://localhost/delete.json


## Test API site ##
A preview to test contact APIs using AJAX. Alternatively, you can use Postman (Chrome add-on).

### Responses ###
#### XML ####
```xml
<?xml version="1.0"?>
<response><code>200</code><res>1</res><msg>OK!</msg></response>
```
#### JSON ####
```json
{"code":200,"res":[1],"msg":"OK!"}
```
Where
* code – HTTP Response Code
* res – response data e.g. contact list
* msg – server message


## About ##

The project is running Apache/Nginx, PHP, MySQL and SQLRelay (http://sqlrelay.sourceforge.net/) provides pooling of connections for MySQL.
This software also offers load balancing and query routing for horizontal application scaling. For this reason, you have created a dedicated
Doctrine \DBAL\Driver\PDOSqlRelay\Driver component that is an adapter for MySQL DBriver DBAL. With that we can use a well-known interface
to communicate with the database for the configured sqlrelay connections.

The database side has created triggers and routines (the dump is in the / database directory). In the connection table, the primary key is a pair
of uid_one identifiers, uid_two. In addition, procedures and triggers guarantee that the smaller value will always be inserted as uid_one and 
larger as uid_two. This always means one entry connects two users back and forth. The whole course is also validated on the application side.

The GET procedure uses HTTP capabilities by setting up eTag, public, max-age response headers. This limits the amount of browser requests
to the server and the processing time in the absence of change in response.

On a separate server, you should run Varnish (http://www.varnish-cache.org/), which requests GET type for a time estimate. This saves server usage.
Varnish also includes load balancing, which allows us to add more servers.

On the PHP side of production you need to run opcache (http://php.net/manual/en/book.opcache.php), which, by creating executable code,
reduces the server's processing time.

The option is to use the sqlrelay/memcache database cache. However, for this type of functionality the above solutions seem to be sufficient.

Request flow:
WebClient → Varnish (Cache) → Server (HTTP Headers) → SQLRelay (Pooling) → MySQL