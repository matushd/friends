<?php

$app->get('/get.{_format}/{uid_one}', "connection.controller:getAction")->bind('uid_one')
        ->assert("_format", "xml|json");
$app->post('/add.{_format}', "connection.controller:addAction")->assert("_format", "xml|json");
$app->delete('/delete.{_format}', "connection.controller:deleteAction")->assert("_format", "xml|json");
$app->get('/test', "connection.controller:testAction");

$app->error(function (\Exception $e, $code) use ($app) {
    return $app->json(array('message' => $e->getMessage(), 'res' => $e->getCode()), $code);
});
