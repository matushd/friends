<?php

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'mysql' => array(
            'host'      => 'localhost',
            'port'      => '9000',
            'dbname'    => 'friendship_db',
            'user'      => 'user',
            'password'  => 'pass',
            'charset'   => 'utf8mb4',
            'tries'  => '0',
            'retrytime'  => '1',
            'debug'  => '0',
            'driverClass' => 'Doctrine\DBAL\Driver\PDOSqlRelay\Driver'
        )
    ),
));
$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\SerializerServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));

$app['repository.connection'] = $app->share(function ($app) {
    return new Emde\Repository\ConnectionRepository($app['dbs']['mysql']);
});

$app['connection.controller'] = $app->share(function() use ($app) {
    return new Emde\Controller\ConnectionController($app['repository.connection'], $app['serializer']);
});